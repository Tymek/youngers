<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>Youngers</title>
	<link href="http://fonts.googleapis.com/css?family=Lato:400,700&amp;subset=latin-ext" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="assets/style.css">
<!--<link rel="stylesheet/less" type="text/css" href="assets/style.less">
	<script>
		less = {
			env: "development",
			async: true,
			fileAsync: true,
			poll: 1000,
			//			functions: {},
			//			dumpLineNumbers: "comments",
			//			relativeUrls: false,
			//			rootpath: ":/a.com/"
		};
	</script>
	<script src="bower_components/less/dist/less.min.js"></script>
	<script>
		less.watch();
	</script>
-->
</head>

<body>
	<header id="top">
		<div class="container">
			<div class="logo col-md-3 col-xs-9 col-sm-4">
				<img src="assets/img/logo.png" alt="Youngers">
			</div>
			<nav id="hamburger" class="visible-xs col-xs-3">
				<button type="button" class="btn btn-default pull-right" aria-label="Otwórz menu">
					<span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
				</button>
			</nav>
			<nav id="menu" class="col-md-8 col-sm-8 col-xs-12 col-sm-offset-0 col-md-offset-1">
				<ul class="row">
					<li class="col-md-3 col-xs-3"><a href="#top"><strong>Start</strong></a></li>
<?php 
	$menu = array(
		"Co robimy?" => "#what",
		"Projekty" => "#portfolio",
		"Kontakt" => "#contact"
	);
	foreach ($menu as $section => $link) { 
?>
					<li class="col-md-3 col-xs-3"><a href="<?= $link ?>"><?= $section ?></a>
<?php }; ?>
				</ul>
			</nav>
		</div>
	</header>
	<div id="home" class="slogan img-block img1">
		<div class="container">
			<div class="col-md-12">
				<div class="margin-fix">
					<span>Youngers</span>
					<br><span>w trzech słowach?</span>
				</div>
				<ul class="margin-fix">
					<li><span>Rzetelność</span></li>
					<li><span>Kreatywność</span></li>
					<li><span>Doświadczenie</span></li>
				</ul>
			</div>
		</div>
	</div>
	<section id="what" class="container">
		<div class="col-md-12">
			<h2>Co robimy?</h2>
			<p>Platforma mediowa Youngers oferuje kompleksową obsługę z&nbsp;zakresu komunikacji marketingowej opartą na kreatywności mediowej oraz długoletnim doświadczeniu zespołu zdobytym w różnych segmentach rynku. Efektywnie uzupełniamy plany mediowe naszych klientów o&nbsp;nośniki OOH, dostarczamy rozwiązania dopasowane do konkretnych grup docelowych, działania odpowiadające na wybrane cele biznesowe oraz komunikacyjne. Pełnimy role doradców, strategów oraz partnerów biznesowych pozwalających osiągać jak najlepsze wyniki. Zapewniamy Klientom kompletną obsługę, krótki czas reakcji, sprawność realizacji oraz pełen monitoring prowadzonych działań.</p>
		</div>
		<div class="col-md-4">
			<div class="text-center">
				<img src="assets/img/ico1.png" alt="">
				<h3>Nośniki</h3>
			</div>
			<p><strong>Sieć Youngsters</strong> obejmuje precyzyjnie dobrane i&nbsp;wyselekcjonowane nośniki OOH w&nbsp;punktach o&nbsp;największym natężeniu ruchu, siatki wielkoformatowe, aranżacje z&nbsp;wykorzystaniem nowych technologii oraz projektowanie i&nbsp;produkcję nośników dedykowanych. Standard. Aranżacje OOH. Digital.</p>
		</div>
		<div class="col-md-4">
			<div class="text-center">
				<img src="assets/img/ico2.png" alt="">
				<h3>Grupy docelowe</h3>
			</div>
			<p><strong>Strategie komunikacji</strong>, które pozwalają na skuteczne, często niestandardowe dotarcie do konkretnych grup docelowych. W naszej ofercie znajduje się kilkadziesiąt zasięgowych punktów styku z&nbsp;konsumentami wspartych zarówno standardowymi nośnikami, jak i&nbsp;dedykowanymi akcjami promocyjnymi. Analiza grupy. Pomysł. Optymalizacja kosztów.</p>
		</div>
		<div class="col-md-4">
			<div class="text-center">
				<img src="assets/img/ico3.png" alt="">
				<h3>Cele</h3>
			</div>
			<p><strong>Planowanie i&nbsp;realizacja</strong> działań marketingowych opartych o&nbsp;ściśle określone cele biznesowe oraz komunikacyjne tj, wsparcie sprzedaży, wzrost świadomości, wprowadzenie produktu na rynek, repozycjonowanie, CSR.</p>
		</div>
	</section>

	<div class="separator img-block img2"><span class="clearfix">&nbsp;</span>
	</div>

	<section id="portfolio" class="container">
		<h2 class="col-md-12">Wybrane projekty</h2>
		<div class="col-md-10 col-md-offset-1">
			<div class="row">
<?php
	$portfolio = json_decode(file_get_contents("portfolio.json"), true)["portfolio"];
	$counter = 0;
	foreach ($portfolio as $item) { ?>
				<div class="col-md-4 col-xs-6">
					<div class="item">
						<img src="assets/img/portfolio/<?= $item["image"] ?>" alt="" class="img-responsive">
						<a href="" class="link" data-toggle="modal" data-target="#portfolio-<?= $counter++ ?>"><span><?= $item["title"] ?></span></a>
					</div>
				</div>
<?php }; ?>
			</div>
		</div>
	</section>

	<div class="separator img-block img3"><span class="clearfix">&nbsp;</span></div>

	<section id="recommendations" class="container">
		<h2>Zaufali nam</h2>
<?php
	$recommendations = array(
		"nivea.png",
		"seat.png",
		"axa.png",
		"mini.png",
		"statoil.png",
		"orange.png",
		"airfrance.png",
		"nc+.png",
		"pge.png"
	);
	$counter = 0;
	foreach ($recommendations as $brand) {
		if ($counter == 0) {
			echo "\t\t\t<div class=\"row\">";
			echo "\t\t\t\t<div class=\"col-md-2 col-xs-3 col-md-offset-1\">";
		} else {
?>
			<div class="col-md-2 col-xs-3">
<?php
		}
?>
				<div class="item col-md-10 col-md-offset-1">
					<img src="assets/img/recommendations/<?= $brand ?>" alt="" class="img-responsive">
				</div>
			</div>
<?php
		$counter++;
		if ($counter == 5) {
			$counter = 0;
			echo "\t\t\t</div>"; ?><?php
			
		}
	}
	if ($counter != 0) {
		echo "\t\t\t</div>";
	}
?>
	</section>

	<div class="separator img-block img4"><span class="clearfix">&nbsp;</span></div>

	<section id="contact" class="container">
		<h2>Kontakt</h2>
		<form class="form-horizontal col-md-7">
			<div class="form-group">
				<label class="col-md-4 control-label" for="textinput">E-mail lub&nbsp;telefon</label>
				<div class="col-md-8">
					<input id="textinput" name="textinput" type="text" placeholder="np. 511627307" class="form-control input-md" required="">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-4 control-label" for="message">Wiadomość</label>
				<div class="col-md-8">
					<textarea class="form-control" id="message" name="message" placeholder="Jestem zainteresowany ofertą i&nbsp;proszę o&nbsp;kontakt."></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-4 control-label" for="message">Weryfikacja</label>
				<div class="col-md-8">
					<div class="g-recaptcha" data-sitekey="6LeIgAgTAAAAAJAZqCsN0uBGDzpKiJs3ZPeZ-SiT"></div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-8 col-md-offset-4">
					<button id="submit" name="submit" class="btn btn-primary">Wyślij</button>
				</div>
			</div>
		</form>
		<div class="col-md-5">
			<div id="map"></div>
		</div>
		<div class="clearfix"></div>
	</section>

	<footer id="footer">
		<div class="container">
			<div class="col-md-7 col-xs-6">
				<div class="col-md-8 col-md-offset-4 col-xs-offset-0">
					e-mail: biuro@youngers.pl
					<br>telefon: +48 511 627 307
				</div>
			</div>
			<address class="col-md-5 col-xs-6">
				Youngers PL Sp. z o.o.<br>
				ul.Sokołowska 9 lok. U4<br>
				01-142 Warszawa
			</address>
		</div>
	</footer>
<?php 
	$counter = 0;
	foreach ($portfolio as $item) { ?>
	<div class="modal fade" id="portfolio-<?= $counter++ ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel"><?= $item["title"] ?></h4>
				</div>
				<div class="modal-body container-fluid">
					<div class="row">
						<?= $item["content"] ?>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
<?php }; ?>
	<script src="assets/jquery.min.js"></script>
	<script async src="assets/functions.js"></script>
	<script async src="assets/modal.js"></script>
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCoFjYrU5tsRfYyJg-Ly-qsCdQsCoM_ZfI&amp;callback=initMap"></script>
	<script async defer src="https://www.google.com/recaptcha/api.js"></script>
</body>

</html>
