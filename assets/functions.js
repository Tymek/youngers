/*global google,$*/

var map;
function initMap() {
	'use strict';
	var settings = {
		scrollwheel: false,
		navigationControl: false,
		streetViewControl: false,
		scaleControl: false,
		panControl: true,
		draggable: false,
		panControlOptions: {
			position: google.maps.ControlPosition.RIGHT_BOTTOM
		},
		zoomControl: true,
		zoomControlOptions: {
			style: google.maps.ZoomControlStyle.LARGE
		},
		navigationControlOptions: {
			style: google.maps.NavigationControlStyle.DEFAULT
		},
		mapTypeControl: true,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		mapTypeControlOptions: {
			style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
			mapTypeIds: [
				google.maps.MapTypeId.ROADMAP,
				google.maps.MapTypeId.HYBRID
			]
		},
		center: {
			lat: 52.2347517,
			lng: 20.959559200000058
		},
		zoom: 15
	}, marker;
								
	map = new google.maps.Map(document.getElementById('map'), settings);
	
	marker = new google.maps.Marker({
		map: map,
		position: new google.maps.LatLng(settings.center.lat, settings.center.lng)
	});
}

$(function () {
	'use strict';
	$("#hamburger button").on('click', function (e) {
		e.preventDefault();
		$("#top").toggleClass("open");
	});
	var $root = $('html, body');
	$('#top nav a').click(function () {
		var href = $.attr(this, 'href'),
			target;
		if (href === "#top") {
			$root.animate({
				scrollTop: $("#home").offset().top
			}, 667, function () {
				window.location.hash = "";
			});
		} else {
			$root.animate({
				scrollTop: $(href).offset().top - $("#top").height()
			}, 667, function () {
				window.location.hash = href;
			});
		}
		return false;
	});
});
